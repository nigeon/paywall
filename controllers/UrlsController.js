var debug = require('debug')('paywall:controller');

const Url = require('../models/Url');

var shortid = require('shortid');

class UrlsController{
  static list(req, res, next){
    Url.find()
      .then(result => {
        res.json({"result": result});
      })
      .catch(err => {
        res.json({"error": err});
      });
  }

  static create(req, res, next){
    Url
      .create(
        { 
          user_id: req.user._id,
          title: req.body.title, 
          url: req.body.url,
          code: shortid.generate()
        }
      )
      .then(result => {
        res.json({"result": result});
      })
      .catch(err => {
        res.json({"error": err});
      });
  }

  static read(req, res, next){
    Url
      .findOne({code: req.params.code})
      .then(result => {
        res.json({"result": result});
      })
      .catch(err => {
        res.json({"error": err});
      });
  }

  static update(req, res, next){
    Url
      .findOneAndUpdate(
        {
          code: req.params.code,
          user_id: req.user._id
        },
        { 
          title: req.body.title, 
          url: req.body.url 
        },
        {new: true, runValidators: true}
      )
      .then(result => {
        res.json({"result": result});
      })
      .catch(err => {
        res.json({"error": err});
      });
  }

  static delete(req, res, next){
    Url
      .remove({
          code: req.params.code,
          user_id: req.user._id
      })
      .then(result => {
        res.json({"result": result});
      })
      .catch(err => {
        res.json({"error": err});
      });
  }
}

module.exports = UrlsController;