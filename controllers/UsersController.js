var debug = require('debug')('paywall:controller');

const User = require('../models/User');

var jwt = require("jwt-simple"); 
const config = require('config');

class UsersController{
  static async signup(req, res, next){
    var data = {
      "email": req.body.email,
      "password": req.body.password
    };

    var user = new User(data);
    try{
      var result = await user.signup();
    }catch(error){
      debug(error);
      return res.sendStatus(500);
    }

    res.json({"result":result});
  }

  static async login(req, res, next){
    var credentials = {
      "email": req.body.email,
      "password": req.body.password
    };

    try{
      var auth_user = await User.login(credentials);
    }catch(error){
      debug(error);
      return res.sendStatus(500);
    }

    if(!auth_user){
      return res.sendStatus(401);
    }

    var token = jwt.encode({"id": auth_user._id}, config.get('JWT').secret);
    res.json({"result": {"token": token}});
  }

  static list(req, res, next){
    User.find()
      .then(result => {
        res.json({"result": result});
      })
      .catch(err => {
        res.json({"error": err});
      });
  }

  static profile(req, res, next){
    var result = req.user;

    res.json({"result": result});
  }
}

module.exports = UsersController;