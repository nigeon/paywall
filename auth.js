var passport = require("passport");
var passportJWT = require("passport-jwt");

const config = require('config');

var User = require("./models/User");  

var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy; 

var params = {  
  secretOrKey: config.get('JWT').secret,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

module.exports = function() {  
  var strategy = new Strategy(params, function(payload, done) {
    User.findById(payload.id)
      .then(auth_user => {
        return done(null, auth_user);
      })
      .catch(err => {
        return done(new Error("User not found"), null);
      });
  });

  passport.use(strategy);

  return {
    initialize: function() {
      return passport.initialize();
    },
    authenticate: function() {
      return passport.authenticate("jwt", config.get('JWT').session);
    }
  };
};