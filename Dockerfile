FROM node:8

# Create app directory
WORKDIR /var/www

# Install yarn
RUN apt-get update && apt-get install -y apt-transport-https
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install yarn

#Install Nodemon for changes monitoring and restarting express
RUN yarn global add nodemon

EXPOSE 3000