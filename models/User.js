var mongoose = require('mongoose');
mongoose.Promise = Promise;

var bcrypt = require('bcrypt');
const saltRounds = 10;

var userSchema = mongoose.Schema({
    email:      { type: String, required: true },
    password:   { type: String, required: true },
    created:    { type: Date, default: Date.now }
});

class User {
  static async login(credentials){
    var found = await this.findOne({ email: credentials.email });
    var password_match = await bcrypt.compare(credentials.password, found.password);
    if(!password_match){
      return false;
    }

    return found;
  }

  async signup(){
    this.password = await bcrypt.hash(this.password, saltRounds);
    return this.save();
  }
}

userSchema.loadClass(User);

module.exports = mongoose.model('User', userSchema);

