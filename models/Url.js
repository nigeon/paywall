var mongoose = require('mongoose');
mongoose.Promise = Promise;

var urlSchema = mongoose.Schema({
    user_id:  { type: String, required: true },
    title:    { type: String, required: true },
    url:      { type: String, required: true },
    code:     { type: String, required: true },
    created:  { type: Date, default: Date.now }
});

module.exports = mongoose.model('Url', urlSchema);

