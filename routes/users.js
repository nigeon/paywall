var express = require('express');
var router = express.Router();

var UsersController = require('../controllers/UsersController');

//Public
router.post('/signup', UsersController.signup);
router.post('/login', UsersController.login);

//Auth required
var auth = require("./../auth.js")();
router.get('/', auth.authenticate(), UsersController.list);
router.get('/profile', auth.authenticate(), UsersController.profile);

module.exports = router;
