var express = require('express');
var router = express.Router();

var UrlsController = require('../controllers/UrlsController');

//Public
router.get('/', UrlsController.list);
router.get('/:code', UrlsController.read);

//Auth required
var auth = require("./../auth.js")();
router.post('/', auth.authenticate(), UrlsController.create);
router.put('/:code', auth.authenticate(), UrlsController.update);
router.delete('/:code', auth.authenticate(), UrlsController.delete);

module.exports = router;
